<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div id="headerWrap" class="wrap">
		<header>
			<nav class="navbar-custom navbar navbar-static-top" role="navigation">
				<!-- Logo e pulsante per barra ridimensionata -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target=".navbar-ex1-collapse">
						<span class="sr-only">Espandi barra di navigazione</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span> <span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#"> <strong>Gestione Pratiche</strong></a>
				</div>

				<!-- Elementi della barra -->
				<div class="collapse navbar-collapse navbar-ex1-collapse">
					<ul class="nav navbar-nav">
						<ul class="nav navbar-nav" style="margin-right: 0px" >
					    	<li ${activeUtenti} class="dropdown">
					    		<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					    			<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
					    				Dati
				    				 <span class="caret" aria-hidden="true"></span>
			    				</a>
				    				
				    			 <ul class="dropdown-menu">	
							    	<li>	
							   			<a style="color: black" href="dati?TG=A&method=getList">Avvocati</a>
							    	</li>	
							    	<li>	
							   			<a style="color: black" href="dati?TG=C&method=getList">Clienti</a>
							    	</li>	
							    	<li>	
							   			<a style="color: black" href="dati?TG=CO&method=getList">Controparti</a>
							    	</li>
							    						   							    		 
							   </ul>
			    			</li>
						</ul> 
						<ul class="nav navbar-nav" style="margin-right: 0px" >
					    	<li ${activeUtenti} class="dropdown">
					    		<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					    			<span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>
					    				&nbsp;Gestione
				    				 <span class="caret" aria-hidden="true"></span>
			    				</a>
				    				
				    			 <ul class="dropdown-menu">
				    			 	<li>	
							   			<a style="color: black" href="gestione?TG=P&method=getList">Pratiche</a>
							    	</li> 		
							    	<li>	
							   			<a style="color: black" href="gestione?TG=A&method=getList">Attivita'</a>
							    	</li>	
							    	<li>	
							   			<a style="color: black" href="gestione?TG=S&method=getList">Spese</a>
							    	</li>
							    	<!--  PER RICHIAMARE SERLVET GESTIONE:
							   			<a style="color: black" href="gestione?method=test">Spese</a>
							   			-->					   							    		 
							   </ul>
			    			</li>
						</ul> 
					</ul>	  
				</div>
			</nav>
		</header>
	</div>

	<div class="page-header">
		<div class="container">
			<h1 style="color: #66a2b4;"> <strong>${title}</strong></h1>
			<c:if test="${not empty error}">
				<div class="alert alert-danger alert-dismissible" role="alert">
				<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>				
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					${error}
				</div>
			</c:if>
			<c:if test="${not empty message}">
				<div class="alert alert-success alert-dismissible" role="alert">
				<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>				
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					${message}
				</div>
			</c:if>
		</div>
	</div>