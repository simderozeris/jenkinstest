<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<jsp:include page="includes/head.jsp"/>

<body background="images/sfondo.png">
<script>
$(function(){
		
	if($('#checkLogin').val() == "KO"){
		
		$('#passwordDIV').addClass("form-group has-error");
		$('#utenteDIV').addClass("form-group has-error");

		$('#wrongutente').text("Nome utente e/o password errata");
		$('#wrongpassword').text("Nome utente e/o password errata");
		
		$("#password").focus(function () {
			$('#passwordDIV').removeClass("form-group has-error");
			$('#wrongpassword').text("");
        });
		
		$("#utente").focus(function () {
			$('#utenteDIV').removeClass("form-group has-error");
			$('#wrongutente').text("");
        });	
	}
	
	 $('#login').submit(function(event) {
		 var error = validate($('#login'));
		 if (error != false) {
		 event.preventDefault();
		 }
	 });
});


function validate(form) {
	var error = false;
	
	form.find('input').each(function(index,input) {
		var name = $(input).attr('name');	
		$('#' + name + 'DIV').removeClass("form-group has-error");
		$('#wrong' + name).text("");
	});

	
	form.find('input').each(function(index,input) {
	var inputValue = $(input).val();
	var required = $(input).data('required');
	var format = $(input).data('format');
	var name = $(input).attr('name');
	
	if (required == true && inputValue == "") {
		
		$('#' + name + 'DIV').addClass("form-group has-error");

		$('#wrong' + name).text("Il campo " + name + " e' obbligatorio");
		
		$("#" + name).focus(function () {
			$('#' + name + 'DIV').removeClass("form-group has-error");
			$('#wrong' + name).text("");
        });
		
		error = "error";
				
	}
});
	
	return error;
}
</script>

<div class="page-header">
		<div class="container">
			<c:if test="${not empty error}">
				<div class="alert alert-danger alert-dismissible" role="alert">
				<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>				
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					${error}
				</div>
			</c:if>
			<c:if test="${not empty message}">
				<div class="alert alert-success alert-dismissible" role="alert">
				<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>				
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					${message}
				</div>
			</c:if>
		</div>
	</div>
	

	<div class="container">
		<div class="row vertical-offset-100">
			<div class="col-md-6 col-md-offset-3">
				
				<div id="panelLogin" class="panel panel-default">
					<div class="panel-heading">
						<h1 class="panel-title">Login</h1>
					</div>
					<div class="panel-body">
						
						<form id="login" action="/JenkinsTest/login" method="POST">
						
							<input id="method" name="method" value="login" data-required='false' type="hidden">
								
							<input id="checkLogin" name="checkLogin" value="${checkLogin}" data-required='false' type="hidden">	

							<fieldset>
								<div id="utenteDIV"  class="form-group">
									<label for="inputUtente" class="control-label">Nome utente</label> 
									<input id="utente" type="text" data-required='true' class="form-control" placeholder="Nome Utente" name="utente"
										type="text">
								</div>
								<label id="wrongutente" style="color: red" class="control-label" for="utenteDIV"></label>																		 
								
								<div id="passwordDIV" class="form-group">
									<label for="inputPassword" class="control-label">Password</label> 
									<input id="password" type="password" data-required='true' class="form-control" placeholder="Password"
										name="password" type="password" value="">
								</div>
								<label id="wrongpassword" style="color: red" class="control-label" for="passwordDIV"></label>																		 
								
								<input class="accedi btn btn-lg btn-primary btn-block" type="submit"
									value="Login">
							</fieldset>
						</form>
					</div>
				</div>				
			</div>
		</div>
	</div>
</body>
</html>