package it.sl.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Key;
import java.util.Properties;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/***
 * Singleton class to read configuration file
 * 
 * Please instantiate it with:
 * 
 * Config config = Config.getInstance();
 * 
 */
public class Config {

	private static Logger logger = LogManager.getLogger(Config.class);
	private static Config config = null;
	private Properties configuration;

	private static String algorithm = "AES";
	//Da cambiare per ogni progetto
	private static byte[] keyValue=new byte[] {123, 85, 20, 59, -10, 43, 64, -87, 37, 102, 120, -120, -54, -5, 78, 29};// your key
	
	private static Key generateKey() throws Exception 
	{
		Key key = new SecretKeySpec(keyValue, algorithm);
		return key;
	}

	public static String encrypt(String clearText) throws Exception
	{
		//generate key
		Key key = generateKey();
		Cipher cipher = Cipher.getInstance(algorithm);
		cipher.init(Cipher.ENCRYPT_MODE, key);
		
		byte[] clearValue = clearText.getBytes("UTF-8");
		byte[] encryptedValue = cipher.doFinal(clearValue);
		String encryptedText = new Base64().encodeAsString(encryptedValue);
		return encryptedText;
	}
	
	public static String decrypt(String encryptedText) throws Exception 
	{
		// generate key
		Key key = generateKey();
		Cipher cipher = Cipher.getInstance(algorithm);
		cipher.init(Cipher.DECRYPT_MODE, key);

		byte[] encryptedValue = new Base64().decode(encryptedText);
		byte[] clearValue = cipher.doFinal(encryptedValue);
		String clearText = new String(clearValue, "UTF-8");
		return clearText;
	}
	
	private Config(String resourcePath) {

		this.configuration = new Properties();
		InputStream in = null;

		try {
			
			in = new FileInputStream(resourcePath);
			this.configuration.load(in);
		} catch (IOException e) {

			logger.error("Errore durante il caricamento della configurazione: " + e.getMessage());
		} finally {

			if (in != null) {

				try { in.close(); } catch (IOException e) { }
			}
		}
	}

	/***
	 * Returns the Config singleton instance
	 * 
	 * @return The Config singleton instance
	 */
	public static Config getInstance() {

		if (config == null) {
			//config = new Config("/jenkinstest.properties");
			//config = new Config("C:\\Users\\simone.derozeris\\Desktop\\jenkinstest.properties");
			config = new Config("/opt/properties/jenkinstest.properties");
		}

		return config;
	}

	/***
	 * Returns the configuration
	 * 
	 * @return The configuration
	 */
	public Properties getConfiguration() {

		return this.configuration;
	}
	
	public String getProperty(String name) {
		
		return this.configuration.getProperty(name);
	}
	
	public String getDecryptedProperty(String name) {
		
		String encryptedText = this.configuration.getProperty(name);
		String decryptedText = null;
		
		try {
			decryptedText = decrypt(encryptedText);
		} catch (Exception e) {
			
			logger.error("Error reading encrypted property: " + name, e);
		}
		
		return decryptedText;
	}
	//* Metodo di cifratura password
	public static void main(String[] args) throws Exception {
		
		//Encrypt password
		String decryptedText = "forzajuve";
		try {
			
			String encryptedText = encrypt(decryptedText);
			logger.info("\"" + decryptedText + "\" ===encrypt===> \"" + encryptedText + "\"");
		} catch (Exception e) {
			
			logger.error("Errore", e);
		}
	}
	//*/
}