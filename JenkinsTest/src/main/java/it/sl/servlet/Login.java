package it.sl.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import it.sl.util.Config;

@WebServlet("/login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Logger logger = Logger.getLogger(Login.class);
	Config c = Config.getInstance();

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {				
			String nomeutente = request.getParameter("utente");
			String password =  request.getParameter("password");
		
			
			if (nomeutente.equals(c.getProperty("admin.nomeutente")) && password.equals(c.getProperty("admin.password"))) {
				
					logger.info("Login effettuato con successo -Username: " + nomeutente + " -Password: " + password);
					request.setAttribute("message", "Login effettuato con successo -Username: " + nomeutente + " -Password: " + password);
			} else {
			
					logger.info("Credenziali errate -Username: " + nomeutente + " -Password: " + password );
					request.setAttribute("error", "Credenziali errate -Username: " + nomeutente + " -Password: " + password);
			}
			getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
		} catch(Exception e) {
			e.printStackTrace();
			getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
		}
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
}

